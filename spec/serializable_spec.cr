require "./spec_helper"

Spectator.describe JSON::Serializable do
  let(valid_json) do
    <<-END_JSON
    {"value":"foo"}
    END_JSON
  end

  let(valid_jsonl) do
    <<-END_JSONL
    {"value":"foo"}
    {"value":"bar"}
    {"value":"baz"}
    END_JSONL
  end

  let(invalid_json) do
    <<-END_JSON
    {42}
    END_JSON
  end

  let(invalid_jsonl) do
    <<-END_JSONL
    {"value":"foo"}
    {"value":"bar"}
    {42}
    {"value":"baz"}
    END_JSONL
  end

  describe ".from_jsonl" do
    subject(entries) { Entry.from_jsonl(input) }
    let(values) { entries.map(&.value) }

    context "with multiple entries" do
      let(input) { valid_jsonl }

      it "yields each entry" do
        expect(values).to eq(%w[foo bar baz])
      end
    end

    context "with one entry" do
      let(input) { valid_json }

      it "yields one entry" do
        expect(values).to eq(%w[foo])
      end
    end

    context "with no entries" do
      let(input) { "" }

      it "doesn't yield" do
        expect(values).to be_empty
      end
    end

    context "with malformed JSON" do
      it "raises a ParseException" do
        expect { Entry.from_jsonl(invalid_json) }.to raise_error(JSON::ParseException)
      end
    end

    context "with partially malformed JSON lines" do
      it "raises a ParseException" do
        expect { Entry.from_jsonl(invalid_jsonl) }.to raise_error(JSON::ParseException)
      end
    end
  end

  describe ".from_jsonl(&)" do
    subject(entries) do
      Array(Entry).new.tap do |array|
        Entry.from_jsonl(input) { |entry| array << entry }
      end
    end

    let(values) { entries.map(&.value) }

    context "with multiple entries" do
      let(input) { valid_jsonl }

      it "yields each entry" do
        expect(values).to eq(%w[foo bar baz])
      end
    end

    context "with one entry" do
      let(input) { valid_json }

      it "yields one entry" do
        expect(values).to eq(%w[foo])
      end
    end

    context "with no entries" do
      let(input) { "" }

      it "doesn't yield" do
        expect(values).to be_empty
      end
    end

    context "with malformed JSON" do
      let(input) { invalid_json }

      it "raises a ParseException" do
        expect { values }.to raise_error(JSON::ParseException)
      end
    end

    context "with partially malformed JSON lines" do
      let(input) { invalid_jsonl }

      # Skip this test for now.
      # The `PullParser` reads (and expects) a valid token after encountering a "start object" token.
      # This puts the parser ahead of the instance its currently reading.
      # In effect, the entry before a malformed one may be skipped.
      skip "yields until the malformed entry is encountered", skip: "Original PullParser reads ahead" do
        values = [] of String
        from_jsonl = ->{
          Entry.from_jsonl(input) do |entry|
            values << entry.value
          end
        }

        expect { from_jsonl.call }.to raise_error(JSON::ParseException)
        expect(values).to eq(%w[foo bar])
      end
    end
  end

  # These tests verify that existing JSON serialization isn't broken.
  describe ".from_json" do
    subject(entry) { Entry.from_json(input) }

    context "with JSON lines" do
      it "raises a ParseException" do
        expect { Entry.from_json(valid_jsonl) }.to raise_error(JSON::ParseException)
      end
    end

    context "with one JSON object" do
      let(input) { valid_json }

      it "reads the object" do
        expect(entry.value).to eq("foo")
      end
    end

    context "with malformed JSON" do
      it "raises a ParseException" do
        expect { Entry.from_json(invalid_json) }.to raise_error(JSON::ParseException)
      end
    end
  end

  describe "#to_json" do
    subject(json) { entry.to_json }
    let(entry) { Entry.new("foo") }

    it "constructs a string with a single JSON object" do
      expect(json).to eq(valid_json)
    end
  end
end
