require "./spec_helper"

Spectator.describe Enumerable do
  let(collection) do
    {
      {value: "foo"},
      {value: "bar"},
      {value: "baz"},
    }
  end

  let(jsonl) do
    <<-END_JSONL
    {"value":"foo"}
    {"value":"bar"}
    {"value":"baz"}

    END_JSONL
  end

  describe "#to_jsonl" do
    subject(output) { collection.to_jsonl }

    it "writes each element to its own line in JSON" do
      expect(output).to eq(jsonl)
    end
  end
end
