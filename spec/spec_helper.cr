require "spectator"
require "../src/jsonl"

# Dummy type for testing JSON::Serializable.
class Entry
  include JSON::Serializable

  getter value : String

  def initialize(@value : String)
  end
end
