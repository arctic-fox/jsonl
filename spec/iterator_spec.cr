require "./spec_helper"

Spectator.describe Iterator do
  let(valid_json) do
    <<-END_JSON
    {"value":"foo"}
    END_JSON
  end

  let(valid_json_array) do
    <<-END_JSON
    [
      {"value":"foo"},
      {"value":"bar"},
      {"value":"baz"}
    ]
    END_JSON
  end

  let(ugly_json_array) { valid_json_array.gsub(/\s+/, "") }

  let(valid_jsonl) do
    <<-END_JSONL
    {"value":"foo"}
    {"value":"bar"}
    {"value":"baz"}
    END_JSONL
  end

  let(invalid_json) do
    <<-END_JSON
    {42}
    END_JSON
  end

  let(invalid_jsonl) do
    <<-END_JSONL
    {"value":"foo"}
    {"value":"bar"}
    {42}
    {"value":"baz"}
    END_JSONL
  end

  let(invalid_json_array) do
    <<-END_JSON
    [
      {"value":"foo"},
      {"value":"bar"},
      {42},
      {"value":"baz"}
    ]
    END_JSON
  end

  describe ".from_jsonl" do
    subject(entries) { Iterator(Entry).from_jsonl(input) }
    let(values) { entries.map(&.value).to_a }

    it "doesn't eagerly iterate" do
      # Give the iterator partially malformed JSON lines.
      # If it iterates eagerly, it will encountered the malformed JSON and raise an error.
      # If it doesn't iterate eagerly, it won't raise an error on initialization.
      iter = Iterator(Entry).from_jsonl(invalid_jsonl)
      expect { iter.next }.to_not raise_error(JSON::ParseException)
    end

    context "with multiple entries" do
      let(input) { valid_jsonl }

      it "iterates each entry" do
        expect(values).to eq(%w[foo bar baz])
      end
    end

    context "with one entry" do
      let(input) { valid_json }

      it "produces one entry" do
        expect(values).to eq(%w[foo])
      end
    end

    context "with no entries" do
      let(input) { "" }

      it "produces nothing" do
        expect(values).to be_empty
      end
    end

    context "with malformed JSON" do
      it "raises a ParseException" do
        expect { Iterator(Entry).from_jsonl(invalid_json).to_a }.to raise_error(JSON::ParseException)
      end
    end

    context "with partially malformed JSON lines" do
      let(input) { invalid_jsonl }

      # Skip this test for now.
      # The `PullParser` reads (and expects) a valid token after encountering a "start object" token.
      # This puts the parser ahead of the instance its currently reading.
      # In effect, the entry before a malformed one may be skipped.
      skip "iterates until the malformed entry is encountered", skip: "Original PullParser reads ahead" do
        iter = Iterator(Entry).from_jsonl(invalid_jsonl)
        expect(iter.next.as(Entry).value).to eq("foo")
        expect(iter.next.as(Entry).value).to eq("bar") # This causes an error.
        expect { iter.next }.to raise_error(JSON::ParseException)
      end
    end
  end

  # These tests verify that existing JSON serialization isn't broken.
  describe ".from_json" do
    subject(iter) { Iterator(Entry).from_json(input) }
    let(values) { iter.map(&.value).to_a }

    context "with JSON lines" do
      it "raises a ParseException" do
        expect { Iterator(Entry).from_json(valid_jsonl) }.to raise_error(JSON::ParseException)
      end
    end

    context "with one JSON object" do
      let(input) { valid_json_array }

      it "reads the object" do
        expect(values).to eq(%w[foo bar baz])
      end
    end

    context "with malformed JSON" do
      it "raises a ParseException" do
        expect { Iterator(Entry).from_json(invalid_json_array).to_a }.to raise_error(JSON::ParseException)
      end
    end
  end

  describe "#to_json" do
    subject(json) { iter.to_json }
    let(iter) { entries.each }
    let(entries) do
      [
        Entry.new("foo"),
        Entry.new("bar"),
        Entry.new("baz"),
      ]
    end

    it "constructs a string with a JSON array" do
      expect(json).to eq(ugly_json_array)
    end
  end
end
