require "json/pull_parser"

module JSON
  # Variation of the default JSON pull parser that supports new-line delimited entries.
  # Instead of stopping after reading one entry, this parser allows multiple back-to-back.
  class LinesPullParser < PullParser
    # Original logic copied from `PullParser`,
    # but modified to emit EOF only if the end of the file has been reached.
    # Otherwise, the first token of the next JSON object is consumed.
    private def next_token_after_value
      case next_token.kind
      when .comma?, .end_array?, .end_object?
        # OK
      else
        # Hit end of file while still processing an array or object.
        unexpected_token unless @object_stack.empty?
      end
    end

    # :ditto:
    private def next_token_after_array_or_object
      unexpected_token unless @object_stack.pop?
      case next_token.kind
      when .comma?, .end_array?, .end_object?
        # OK
      when .eof?
        # Hit end of file while still processing an array or object.
        unexpected_token unless @object_stack.empty?
      else
        # New entry
      end
    end
  end
end
