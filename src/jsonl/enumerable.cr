# Adds methods for writing serializable objects to JSON lines.
module Enumerable
  # Constructs a string with each element of this object on their own line in JSON.
  # One line per object where each line contains a valid JSON document.
  def to_jsonl : String
    String.build do |str|
      to_jsonl(str)
    end
  end

  # Writes each element of this object to their own line in JSON.
  # One line is written per object where each line contains a valid JSON document.
  def to_jsonl(io : IO) : Nil
    builder = JSON::Builder.new(io)
    each do |entry|
      builder.document do
        entry.to_json(builder)
      end
      io.puts
    end
  end
end
