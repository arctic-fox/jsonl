require "./lines_pull_parser"

module JSON
  # Adds methods for reading serializable objects from JSON lines.
  # Include in a type that is serializable via JSON.
  #
  # ```
  # class Thing
  #   include JSON::Serializable
  # end
  # ```
  #
  # The `.from_jsonl` method reads instances from a JSON lines formatted input.
  # There are two forms, one that takes a block and one that does not.
  # Both accept an input, which can be either a `String` or `IO` (same as `.from_json`).
  #
  # The form that takes a block yields for every instance read from the input.
  #
  # ```
  # Thing.from_jsonl(io) do |thing|
  #   p thing
  # end
  # ```
  #
  # The other form returns an array of all instances read.
  # This is not recommended if the number of JSON entries is large or unknown.
  #
  # ```
  # things = Thing.from_jsonl(io)
  # ```
  module Serializable
    macro included
      # Parses a `String` or `IO` to extract multiple instances in JSON format.
      # Returns an array containing all of the entries.
      # This method is not recommended for large or unknown quantities of entries.
      def self.from_jsonl(string_or_io)
        Array(self).new.tap do |array|
          from_jsonl(string_or_io) { |obj| array << obj }
        end
      end

      # Parses a `String` or `IO` to extract multiple instances in JSON format.
      # Yields each entry.
      def self.from_jsonl(string_or_io, & : self -> _)
        pull = JSON::LinesPullParser.new(string_or_io)
        until pull.kind.eof?
          yield new(pull)
        end
      end
    end
  end
end
