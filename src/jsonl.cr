require "json"

module JSON
  JSONL_VERSION = "0.2.0"
end

require "./jsonl/**"
